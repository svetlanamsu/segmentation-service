module.exports = {
  devServer:{
    proxy:{
      '^/api/':{
        target:'http://segmentation:8081/', 
        secure: false,
        pathRewrite:{
          '/api/*': '/'
        }
      }
    }
  },
  transpileDependencies: [
    'vuetify'
  ]
}
