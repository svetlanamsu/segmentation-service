from enum import Enum
from predictor.Linear import Linear
from predictor.HLSTMModel import HLSTMModel


class ModelName(str, Enum):
    hlstm = "hlstm"
    linear = "linear"


models_map = {
    ModelName.linear: Linear,
    ModelName.hlstm: HLSTMModel
}
