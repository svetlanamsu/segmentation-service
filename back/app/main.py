from typing import Optional

from fastapi import FastAPI
from api import api
import uvicorn

app = FastAPI()

app.include_router(api.router)


if __name__ == "__main__":
    # for debugging
    uvicorn.run(app, host="0.0.0.0", port=8082)