import abc
import re
import torch
from torch.functional import F
from navec import Navec
from razdel import sentenize
import numpy as np
from pymorphy2.tokenizers import simple_word_tokenize

from logger.handlers import stdout_handler
import logging
logger = logging.Logger('base_model', level=logging.DEBUG)
logger.addHandler(stdout_handler())


class BaseModel:

    @abc.abstractmethod
    def load_model(self):
        pass

    @abc.abstractmethod
    def predict(self, text):
        pass


class SimpleTorchModel(BaseModel):
    def __init__(self):
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.path_navec = 'predictor/static/navec_hudlit_v1_12B_500K_300d_100q.tar'
        self.navec = Navec.load(self.path_navec)
        self.embed_size = 300 # self.navec.as_gensim.vector_size
        self.max_length = 128
        self.instance = None

    def load_model(self):
        pass

    def predict(self, text):
        # 1. разделить текст на предложения
        sentences = self.split_sentences(text)
        # 2. получить в лоадер
        inputs = self.get_embeddings(sentences)
        # 3. получить предсказание
        predictions = self.get_predictions(inputs)
        # 4. разбить исходный текст на сегменты в соответствии с исходным текстом
        parts = list()
        start = 0
        stop = len(text)
        for sentence, label in zip(sentences[:-1], predictions[:-1]):
            if label == 1:
                parts.append(text[start:sentence.stop])
                start = sentence.stop + 1
        parts.append(text[start:])
        return parts, predictions

    def get_predictions(self, inputs):
        self.instance.eval()
        input_embeds = inputs.to(self.device)
        self.instance.to(self.device)
        output = self.instance(input_embeds)
        if len(output.shape) == 1:
            output = torch.unsqueeze(output, 0)
        predictions = F.softmax(output, dim=1).argmax(dim=1).to('cpu').numpy()
        return list(predictions)

    def get_embeddings(self, sentences):
        inputs = list()  # массив вложений
        max_length = 0
        for sentence in sentences:
            s = sentence.text
            pure_text = re.sub(r'[^А-Яа-яЁё\s]', ' ', s).strip()
            embeds = [
                         self.navec[token]
                         for token in simple_word_tokenize(pure_text.lower()) if token in self.navec
                     ][:self.max_length]
            if len(embeds) == 0:
                embeds.append(self.navec['<unk>'])
            max_length = max(max_length, len(embeds))
            inputs.append(np.array(embeds))
        # TODO: привести к одинаковой длине предложений
        input_embeds = np.zeros((len(inputs), max_length, self.embed_size))
        for i, item in enumerate(inputs):
            input_embeds[i][:item.shape[0], :] = item
        return torch.FloatTensor(input_embeds)

    @staticmethod
    def split_sentences(text):
        return list(sentenize(text))
